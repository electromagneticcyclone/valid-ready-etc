// Asynchronous reset here is needed for the FPGA board we use

`include "config.vh"

module fpga_top
(
  input         adc_clk_10,
  input         max10_clk1_50,
  input         max10_clk2_50,

  input  [ 1:0] key,
  input  [ 9:0] sw,
  output [ 9:0] led,

  output [ 7:0] hex0,
  output [ 7:0] hex1,
  output [ 7:0] hex2,
  output [ 7:0] hex3,
  output [ 7:0] hex4,
  output [ 7:0] hex5,

  output        vga_hs,
  output        vga_vs,
  output [ 3:0] vga_r,
  output [ 3:0] vga_g,
  output [ 3:0] vga_b,

  inout  [35:0] gpio
);

  //--------------------------------------------------------------------------
  // Unused pins

  assign led    = '0;

  assign vga_hs = '0;
  assign vga_vs = '0;
  assign vga_r  = '0;
  assign vga_g  = '0;
  assign vga_b  = '0;

  //--------------------------------------------------------------------------
  // Clock and reset

  wire clk = max10_clk1_50;
  wire rst = sw [9];

  //--------------------------------------------------------------------------

  `ifdef SIMULATION

    wire slow_clk = clk;

  `else

    wire slow_clk_raw, slow_clk;

    slow_clk_gen # (26) i_slow_clk_gen (.slow_clk_raw (slow_clk_raw), .*);

    // "global" is Intel FPGA-specific primitive to route
    // a signal coming from data into clock tree

    global i_global (.in (slow_clk_raw), .out (slow_clk));

  `endif  // `ifdef SIMULATION

  //--------------------------------------------------------------------------

  localparam fifo_width = 4, fifo_depth = 4;

  wire [fifo_width - 1:0] write_data;
  wire [fifo_width - 1:0] read_data;
  wire empty, full;

  wire push = ~ full  & ~ key [0];
  wire pop  = ~ empty & ~ key [1];

  wire [31:0] debug;

  //--------------------------------------------------------------------------

  `ifdef __ICARUS__

    logic [fifo_width - 1:0] write_data_const_array [0:2 ** fifo_width - 1];

    assign write_data_const_array [ 0] = 4'h2;
    assign write_data_const_array [ 1] = 4'h6;
    assign write_data_const_array [ 2] = 4'hd;
    assign write_data_const_array [ 3] = 4'hb;
    assign write_data_const_array [ 4] = 4'h7;
    assign write_data_const_array [ 5] = 4'he;
    assign write_data_const_array [ 6] = 4'hc;
    assign write_data_const_array [ 7] = 4'h4;
    assign write_data_const_array [ 8] = 4'h1;
    assign write_data_const_array [ 9] = 4'h0;
    assign write_data_const_array [10] = 4'h9;
    assign write_data_const_array [11] = 4'ha;
    assign write_data_const_array [12] = 4'hf;
    assign write_data_const_array [13] = 4'h5;
    assign write_data_const_array [14] = 4'h8;
    assign write_data_const_array [15] = 4'h3;

  `else

    // New SystemVerilog syntax for array assignment

    wire [fifo_width - 1:0] write_data_const_array [0:2 ** fifo_width - 1]
      = '{ 4'h2, 4'h6, 4'hd, 4'hb, 4'h7, 4'he, 4'hc, 4'h4,
           4'h1, 4'h0, 4'h9, 4'ha, 4'hf, 4'h5, 4'h8, 4'h3 };

  `endif

  //--------------------------------------------------------------------------

  wire [fifo_width - 1:0] write_data_index;

  counter_with_enable # (fifo_width) i_counter
  (
    .clk    (slow_clk),
    .enable (push),
    .cnt    (write_data_index),
    .*
  );

  assign write_data = write_data_const_array [write_data_index];

  //--------------------------------------------------------------------------

  flip_flop_fifo_empty_full_optimized
  # (
    .width (fifo_width),
    .depth (fifo_depth)
  )
  i_fifo (.clk (slow_clk), .*);

  wire [3:0] wr_ptr = debug [19:16];
  wire [3:0] rd_ptr = debug [ 3: 0];

  localparam sign_full  = 8'b11111110,
             sign_empty = 8'b11110111,
             no_sign    = 8'b11111111;

  assign { hex5, hex4 } = { no_sign, no_sign };

  wire [7:0] hex3_pre;

  seven_segment_digit hex3_display
  (
    .dig (write_data),
    .dot (1'b0),
    .hgfedcba (hex3_pre)
  );

  assign hex3 = full ? sign_full : hex3_pre;

  seven_segment_digit hex2_display
  (
    .dig (wr_ptr),
    .dot (1'b0),
    .hgfedcba (hex2)
  );

  seven_segment_digit hex1_display
  (
    .dig (rd_ptr),
    .dot (1'b0),
    .hgfedcba (hex1)
  );

  wire [7:0] hex0_pre;

  seven_segment_digit hex0_display
  (
    .dig (read_data),
    .dot (1'b0),
    .hgfedcba (hex0_pre)
  );

  assign hex0 = empty ? sign_empty : hex0_pre;

endmodule
