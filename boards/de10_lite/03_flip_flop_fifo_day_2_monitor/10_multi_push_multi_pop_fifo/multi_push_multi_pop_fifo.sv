module multi_push_multi_pop_fifo
# (
  parameter w = 13,  // fifo width
            d = 19,  // fifo_depth
            n = 2,   // max number of pushes or pops
            nw = $clog2 (n + 1)
)
(
  input                      clk,
  input                      rst,
  input  [nw - 1:0]          push,
  input  [n  - 1:0][w - 1:0] push_data,
  input  [nw - 1:0]          pop,
  output [n  - 1:0][w - 1:0] pop_data,
  output [nw - 1:0]          can_push,  // how many items can I push
  output [nw - 1:0]          can_pop
);

  // TODO: Implement the whole example
  // with testbench and FPGA demo

endmodule
