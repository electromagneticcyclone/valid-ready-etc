// Asynchronous reset here is needed for one of FPGA boards we use

`include "config.vh"

module flow_controlled_register
# (
  parameter width = 8
)
(
  input                      clk,
  input                      rst,

  input                      up_valid,
  output                     up_ready,
  input        [width - 1:0] up_data,

  output logic               down_valid,
  input                      down_ready,
  output logic [width - 1:0] down_data
);

  always_ff @ (posedge clk)
    if (up_ready)
      down_data <= up_data;

  always_ff @ (posedge clk or posedge rst)
    if (rst)
      down_valid <= 1'b0;
    else if (up_ready)
      down_valid <= up_valid;

  assign up_ready = ~ down_valid | down_ready;

endmodule
