// Asynchronous reset here is needed for the FPGA board we use

`include "config.vh"

module fpga_top
# (
  parameter width = 4, depth = 4
)
(
  input               adc_clk_10,
  input               max10_clk1_50,
  input               max10_clk2_50,

  input        [ 1:0] key,
  input        [ 9:0] sw,
  output       [ 9:0] led,

  output       [ 7:0] hex0,
  output logic [ 7:0] hex1,
  output       [ 7:0] hex2,
  output       [ 7:0] hex3,
  output       [ 7:0] hex4,
  output       [ 7:0] hex5,

  output              vga_hs,
  output              vga_vs,
  output       [ 3:0] vga_r,
  output       [ 3:0] vga_g,
  output       [ 3:0] vga_b,

  inout        [35:0] gpio
);

  //--------------------------------------------------------------------------
  // Unused pins

  assign led    = '0;

  assign vga_hs = '0;
  assign vga_vs = '0;
  assign vga_r  = '0;
  assign vga_g  = '0;
  assign vga_b  = '0;

  //--------------------------------------------------------------------------
  // Clock and reset

  wire clk = max10_clk1_50;
  wire rst = sw [9];

  //--------------------------------------------------------------------------

  `ifdef SIMULATION

    wire slow_clk = clk;

  `else

    wire slow_clk_raw, slow_clk;

    slow_clk_gen # (26) i_slow_clk_gen (.slow_clk_raw (slow_clk_raw), .*);

    // "global" is Intel FPGA-specific primitive to route
    // a signal coming from data into clock tree

    global i_global (.in (slow_clk_raw), .out (slow_clk));

  `endif  // `ifdef SIMULATION

  //--------------------------------------------------------------------------

  wire               a_valid   = ~ key [0];  // Top key is pressed
  wire               a_ready;
  wire [width - 1:0] a_data;

  wire               b_valid   = ~ key [1];  // Bottom key is pressed
  wire               b_ready;
  wire [width - 1:0] b_data;

  // Note that sum ready is ON by default

  wire               sum_valid;
  wire               sum_ready = ~ sw [0];  // Switch is not ON
  wire [width - 1:0] sum_data;

  //--------------------------------------------------------------------------

  `ifdef __ICARUS__

    logic [width - 1:0] data_const_array [0:2 ** width - 1];

    assign data_const_array [ 0] = 4'h2;
    assign data_const_array [ 1] = 4'h6;
    assign data_const_array [ 2] = 4'hd;
    assign data_const_array [ 3] = 4'hb;
    assign data_const_array [ 4] = 4'h7;
    assign data_const_array [ 5] = 4'he;
    assign data_const_array [ 6] = 4'hc;
    assign data_const_array [ 7] = 4'h4;
    assign data_const_array [ 8] = 4'h1;
    assign data_const_array [ 9] = 4'h0;
    assign data_const_array [10] = 4'h9;
    assign data_const_array [11] = 4'ha;
    assign data_const_array [12] = 4'hf;
    assign data_const_array [13] = 4'h5;
    assign data_const_array [14] = 4'h8;
    assign data_const_array [15] = 4'h3;

  `else

    // New SystemVerilog syntax for array assignment

    wire [width - 1:0] data_const_array [0:2 ** width - 1]
      = '{ 4'h2, 4'h6, 4'hd, 4'hb, 4'h7, 4'he, 4'hc, 4'h4,
           4'h1, 4'h0, 4'h9, 4'ha, 4'hf, 4'h5, 4'h8, 4'h3 };

  `endif

  //--------------------------------------------------------------------------

  wire [width - 1:0] a_data_index;

  counter_with_enable # (width) i_a_counter
  (
    .clk    (slow_clk),
    .enable (a_valid & a_ready),
    .cnt    (a_data_index),
    .*
  );

  assign a_data = data_const_array [a_data_index];

  //--------------------------------------------------------------------------

  wire [width - 1:0] b_data_index;

  counter_with_enable # (width) i_b_counter
  (
    .clk    (slow_clk),
    .enable (b_valid & b_ready),
    .cnt    (b_data_index),
    .*
  );

  assign b_data = data_const_array [b_data_index + 1];

  //--------------------------------------------------------------------------

  a_plus_b_using_fifos
  # (.width (width), .depth (depth))
  a_plus_b (.clk (slow_clk), .*);

  //--------------------------------------------------------------------------

  localparam sign_ready_a   = 8'b10111111,
             sign_ready_b   = 8'b11111110,
             sign_ready_sum = 8'b11110111,
             sign_nothing   = 8'b11111111;

  assign { hex5, hex4 } = { sign_nothing, sign_nothing };

  always_comb
    begin
      hex1 = sign_nothing;

      if ( a_ready   ) hex1 &= sign_ready_a;
      if ( b_ready   ) hex1 &= sign_ready_b;
      if ( sum_ready ) hex1 &= sign_ready_sum;
    end

  seven_segment_digit hex3_display
  (
    .dig (a_data),
    .dot (1'b0),
    .hgfedcba (hex3)
  );

  seven_segment_digit hex2_display
  (
    .dig (b_data),
    .dot (1'b0),
    .hgfedcba (hex2)
  );

  wire [7:0] hex0_pre;

  seven_segment_digit hex0_display
  (
    .dig (sum_data),
    .dot (1'b0),
    .hgfedcba (hex0_pre)
  );

  assign hex0 = sum_valid ? hex0_pre : sign_nothing;

endmodule
