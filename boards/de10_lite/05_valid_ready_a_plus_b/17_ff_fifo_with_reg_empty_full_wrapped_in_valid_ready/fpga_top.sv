// Asynchronous reset here is needed for the FPGA board we use

`include "config.vh"

module fpga_top
# (
  parameter width = 4, depth = 4
)
(
  input         adc_clk_10,
  input         max10_clk1_50,
  input         max10_clk2_50,

  input  [ 1:0] key,
  input  [ 9:0] sw,
  output [ 9:0] led,

  output [ 7:0] hex0,
  output [ 7:0] hex1,
  output [ 7:0] hex2,
  output [ 7:0] hex3,
  output [ 7:0] hex4,
  output [ 7:0] hex5,

  output        vga_hs,
  output        vga_vs,
  output [ 3:0] vga_r,
  output [ 3:0] vga_g,
  output [ 3:0] vga_b,

  inout  [35:0] gpio
);

  //--------------------------------------------------------------------------
  // Unused pins

  assign led    = '0;

  assign vga_hs = '0;
  assign vga_vs = '0;
  assign vga_r  = '0;
  assign vga_g  = '0;
  assign vga_b  = '0;

  //--------------------------------------------------------------------------
  // Clock and reset

  wire clk = max10_clk1_50;
  wire rst = sw [9];

  //--------------------------------------------------------------------------

  `ifdef SIMULATION

    wire slow_clk = clk;

  `else

    wire slow_clk_raw, slow_clk;

    slow_clk_gen # (26) i_slow_clk_gen (.slow_clk_raw (slow_clk_raw), .*);

    // "global" is Intel FPGA-specific primitive to route
    // a signal coming from data into clock tree

    global i_global (.in (slow_clk_raw), .out (slow_clk));

  `endif  // `ifdef SIMULATION

  //--------------------------------------------------------------------------

  // Upstream

  wire               up_valid = ~ key [0];  // Top key is pressed
  wire               up_ready;
  wire [width - 1:0] up_data;

  // Downstream

  // Note that downstream ready is ON by default

  wire               down_valid;
  wire               down_ready = key [1];  // Bottom key is not pressed
  wire [width - 1:0] down_data;

  //--------------------------------------------------------------------------

  `ifdef __ICARUS__

    logic [width - 1:0] up_data_const_array [0:2 ** width - 1];

    assign up_data_const_array [ 0] = 4'h2;
    assign up_data_const_array [ 1] = 4'h6;
    assign up_data_const_array [ 2] = 4'hd;
    assign up_data_const_array [ 3] = 4'hb;
    assign up_data_const_array [ 4] = 4'h7;
    assign up_data_const_array [ 5] = 4'he;
    assign up_data_const_array [ 6] = 4'hc;
    assign up_data_const_array [ 7] = 4'h4;
    assign up_data_const_array [ 8] = 4'h1;
    assign up_data_const_array [ 9] = 4'h0;
    assign up_data_const_array [10] = 4'h9;
    assign up_data_const_array [11] = 4'ha;
    assign up_data_const_array [12] = 4'hf;
    assign up_data_const_array [13] = 4'h5;
    assign up_data_const_array [14] = 4'h8;
    assign up_data_const_array [15] = 4'h3;

  `else

    // New SystemVerilog syntax for array assignment

    wire [width - 1:0] up_data_const_array [0:2 ** width - 1]
      = '{ 4'h2, 4'h6, 4'hd, 4'hb, 4'h7, 4'he, 4'hc, 4'h4,
           4'h1, 4'h0, 4'h9, 4'ha, 4'hf, 4'h5, 4'h8, 4'h3 };

  `endif

  //--------------------------------------------------------------------------

  wire [width - 1:0] up_data_index;

  counter_with_enable # (width) i_counter
  (
    .clk    (slow_clk),
    .enable (up_valid & up_ready),
    .cnt    (up_data_index),
    .*
  );

  assign up_data = up_data_const_array [up_data_index];

  //--------------------------------------------------------------------------

  ff_fifo_wrapped_in_valid_ready
  # (.width (width), .depth (depth))
  wrapped_fifo (.clk (slow_clk), .*);

  //--------------------------------------------------------------------------

  localparam sign_valid   = 8'b11111110,
             sign_ready   = 8'b10111111,
             sign_nothing = 8'b11111111;

  function [7:0] valid_ready_to_hgfedcba (logic valid, ready);

    case ({ valid, ready })
    2'b00: return sign_nothing;
    2'b10: return sign_valid;
    2'b01: return sign_ready;
    2'b11: return sign_valid & sign_ready;
    endcase

  endfunction

  //--------------------------------------------------------------------------

  assign { hex5, hex4 } = { sign_nothing, sign_nothing };

  assign hex2 = valid_ready_to_hgfedcba ( up_valid   , up_ready   );
  assign hex1 = valid_ready_to_hgfedcba ( down_valid , down_ready );

  seven_segment_digit hex3_display
  (
    .dig (up_data),
    .dot (1'b0),
    .hgfedcba (hex3)
  );

  wire [7:0] hex0_pre;

  seven_segment_digit hex0_display
  (
    .dig (down_data),
    .dot (1'b0),
    .hgfedcba (hex0_pre)
  );

  assign hex0 = down_valid ? hex0_pre : sign_nothing;

endmodule
