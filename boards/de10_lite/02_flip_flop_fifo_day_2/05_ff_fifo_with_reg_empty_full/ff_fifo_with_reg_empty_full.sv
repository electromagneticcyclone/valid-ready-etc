// Asynchronous reset here is needed for one of FPGA boards we use

`include "config.vh"

module ff_fifo_with_reg_empty_full
# (
  parameter width = 8, depth = 10
)
(
  input                      clk,
  input                      rst,
  input                      push,
  input                      pop,
  input        [width - 1:0] write_data,
  output       [width - 1:0] read_data,
  output logic               empty,
  output logic               full

  `ifdef SYNTHESIS
  ,
  output       [       31:0] debug
  `endif
);

  //--------------------------------------------------------------------------

  localparam pointer_width = $clog2 (depth),
             counter_width = $clog2 (depth + 1);

  localparam [counter_width - 1:0] max_ptr = counter_width' (depth - 1);

  //--------------------------------------------------------------------------

  logic [pointer_width - 1:0] wr_ptr_d, rd_ptr_d, wr_ptr_q, rd_ptr_q;
  logic empty_d, full_d;
  logic [width - 1:0] data [0: depth - 1];

  //--------------------------------------------------------------------------

  `ifdef SYNTHESIS
  assign debug [31:16] = 16' (wr_ptr_q);
  assign debug [15:00] = 16' (rd_ptr_q);
  `endif

  // TODO: Implement FIFO where empty and full flags are registered

endmodule
