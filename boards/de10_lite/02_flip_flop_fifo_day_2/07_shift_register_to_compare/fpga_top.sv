// Asynchronous reset here is needed for the FPGA board we use

`include "config.vh"

module fpga_top
(
  input         adc_clk_10,
  input         max10_clk1_50,
  input         max10_clk2_50,

  input  [ 1:0] key,
  input  [ 9:0] sw,
  output [ 9:0] led,

  output [ 7:0] hex0,
  output [ 7:0] hex1,
  output [ 7:0] hex2,
  output [ 7:0] hex3,
  output [ 7:0] hex4,
  output [ 7:0] hex5,

  output        vga_hs,
  output        vga_vs,
  output [ 3:0] vga_r,
  output [ 3:0] vga_g,
  output [ 3:0] vga_b,

  inout  [35:0] gpio
);

  //--------------------------------------------------------------------------
  // Unused pins

  assign led    = '0;

  assign vga_hs = '0;
  assign vga_vs = '0;
  assign vga_r  = '0;
  assign vga_g  = '0;
  assign vga_b  = '0;

  //--------------------------------------------------------------------------
  // Clock and reset

  wire clk = max10_clk1_50;
  wire rst = sw [9];

  //--------------------------------------------------------------------------

  `ifdef SIMULATION

    wire slow_clk = clk;

  `else

    wire slow_clk_raw, slow_clk;

    slow_clk_gen # (26) i_slow_clk_gen (.slow_clk_raw (slow_clk_raw), .*);

    // "global" is Intel FPGA-specific primitive to route
    // a signal coming from data into clock tree

    global i_global (.in (slow_clk_raw), .out (slow_clk));

  `endif  // `ifdef SIMULATION

  //--------------------------------------------------------------------------

  localparam width = 4, depth = 4;

  wire               in_valid = (key != 2'b11);  // Any key is pressed
  wire [width - 1:0] in_data;

  wire               out_valid;
  wire [width - 1:0] out_data;

  wire [depth - 1:0]              debug_valid;
  wire [depth - 1:0][width - 1:0] debug_data;

  //--------------------------------------------------------------------------

  `ifdef __ICARUS__

    logic [width - 1:0] in_data_const_array [0:2 ** width - 1];

    assign in_data_const_array [ 0] = 4'h2;
    assign in_data_const_array [ 1] = 4'h6;
    assign in_data_const_array [ 2] = 4'hd;
    assign in_data_const_array [ 3] = 4'hb;
    assign in_data_const_array [ 4] = 4'h7;
    assign in_data_const_array [ 5] = 4'he;
    assign in_data_const_array [ 6] = 4'hc;
    assign in_data_const_array [ 7] = 4'h4;
    assign in_data_const_array [ 8] = 4'h1;
    assign in_data_const_array [ 9] = 4'h0;
    assign in_data_const_array [10] = 4'h9;
    assign in_data_const_array [11] = 4'ha;
    assign in_data_const_array [12] = 4'hf;
    assign in_data_const_array [13] = 4'h5;
    assign in_data_const_array [14] = 4'h8;
    assign in_data_const_array [15] = 4'h3;

  `else

    // New SystemVerilog syntax for array assignment

    wire [width - 1:0] in_data_const_array [0:2 ** width - 1]
      = '{ 4'h2, 4'h6, 4'hd, 4'hb, 4'h7, 4'he, 4'hc, 4'h4,
           4'h1, 4'h0, 4'h9, 4'ha, 4'hf, 4'h5, 4'h8, 4'h3 };

  `endif

  //--------------------------------------------------------------------------

  wire [width - 1:0] in_data_index;

  counter_with_enable # (width) i_counter
  (
    .clk    (slow_clk),
    .enable (in_valid),
    .cnt    (in_data_index),
    .*
  );

  assign in_data = in_data_const_array [in_data_index];

  //--------------------------------------------------------------------------

  shift_register_with_valid_and_debug
  # (
    .width (width),
    .depth (depth)
  )
  i_shift_register (.clk (slow_clk), .*);

  //--------------------------------------------------------------------------

  seven_segment_digit hex5_display
  (
    .dig (in_data),
    .dot (1'b0),
    .hgfedcba (hex5)
  );

  //--------------------------------------------------------------------------

  localparam sign_empty_entry = 8'b01111111,
             sign_nothing     = 8'b11111111;

  //--------------------------------------------------------------------------

  `define HEX_DISPLAY(i)                        \
                                                \
    generate                                    \
      if (i >= depth)                           \
      begin                                     \
        assign hex``i = sign_nothing;           \
      end                                       \
      else                                      \
      begin                                     \
        wire [7:0] hex``i``_pre;                \
                                                \
        seven_segment_digit hex``i``_display    \
        (                                       \
          .dig (debug_data [depth - 1 - i]),    \
          .dot (1'b0),                          \
          .hgfedcba (hex``i``_pre)              \
        );                                      \
                                                \
        assign hex``i                           \
          = debug_valid [depth - 1 - i] ?       \
              hex``i``_pre : sign_empty_entry;  \
      end                                       \
    endgenerate                                 \

  //--------------------------------------------------------------------------

  `HEX_DISPLAY (4)
  `HEX_DISPLAY (3)
  `HEX_DISPLAY (2)
  `HEX_DISPLAY (1)
  `HEX_DISPLAY (0)

  `undef HEX_DISPLAY

endmodule
