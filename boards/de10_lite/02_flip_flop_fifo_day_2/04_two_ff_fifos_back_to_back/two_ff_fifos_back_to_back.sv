// Asynchronous reset here is needed for one of FPGA boards we use

`include "config.vh"

module two_ff_fifos_back_to_back
# (
  parameter width = 8, depth = 5
)
(
  input                clk,
  input                rst,
  input                push,
  input                pop,
  input  [width - 1:0] write_data,
  output [width - 1:0] read_data,
  output               empty,
  output               full

  `ifdef SYNTHESIS
  ,
  output [63:0]        debug
  `endif
);

  // TODO: Instantiate two FIFOs back to back to each other.
  // They should work as a single FIFO.

endmodule
