// Asynchronous reset here is needed for the FPGA board we use

`include "config.vh"

`ifndef SIMULATION

module fpga_top
(
  input         adc_clk_10,
  input         max10_clk1_50,
  input         max10_clk2_50,

  input  [ 1:0] key,
  input  [ 9:0] sw,
  output [ 9:0] led,

  output [ 7:0] hex0,
  output [ 7:0] hex1,
  output [ 7:0] hex2,
  output [ 7:0] hex3,
  output [ 7:0] hex4,
  output [ 7:0] hex5,

  output        vga_hs,
  output        vga_vs,
  output [ 3:0] vga_r,
  output [ 3:0] vga_g,
  output [ 3:0] vga_b,

  inout  [35:0] gpio
);

  //--------------------------------------------------------------------------
  // Unused pins

  assign led    = '0;

  assign vga_hs = '0;
  assign vga_vs = '0;
  assign vga_r  = '0;
  assign vga_g  = '0;
  assign vga_b  = '0;

  //--------------------------------------------------------------------------
  // Clock and reset

  wire clk = max10_clk1_50;
  wire rst = sw [9];

  //--------------------------------------------------------------------------

  wire slow_clk_raw, slow_clk;

  slow_clk_gen # (26) i_slow_clk_gen (.slow_clk_raw (slow_clk_raw), .*);

  // "global" is Intel FPGA-specific primitive to route
  // a signal coming from data into clock tree

  global i_global (.in (slow_clk_raw), .out (slow_clk));

  //--------------------------------------------------------------------------

  localparam width = 4, depth = 5;

  wire               in_valid = ~ key [0];
  wire [width - 1:0] in_data;

  wire               out_valid;
  wire [width - 1:0] out_data;

  wire [depth - 1:0]              debug_valid;
  wire [depth - 1:0][width - 1:0] debug_data;

  //--------------------------------------------------------------------------

  counter # (width) i_counter (.clk (slow_clk), .cnt (in_data), .*);

  shift_register_with_valid_and_debug
  # (
    .width (width),
    .depth (depth)
  )
  i_shift_register (.clk (slow_clk), .*);

  //--------------------------------------------------------------------------

  seven_segment_digit hex5_display
  (
    .dig (in_data),
    .dot (1'b0),
    .hgfedcba (hex5)
  );

  //--------------------------------------------------------------------------

  localparam sign_empty = 8'b11111111;

  // TODO: Implement rest of the output to hex0..4

endmodule

`endif  // ifndef SIMULATION
