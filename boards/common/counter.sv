// Asynchronous reset here is needed for one of FPGA boards we use

`include "config.vh"

module counter
# (
  parameter w = 24
)
(
  input  clk,
  input  rst,
  output logic [w - 1:0] cnt
);

  counter_with_enable # (w) i (.enable ('1), .*);

endmodule
