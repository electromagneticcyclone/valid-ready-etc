// Asynchronous reset here is needed for one of FPGA boards we use

`include "config.vh"

module strobe_gen
# (
  parameter w = 24
)
(
  input  clk,
  input  rst,
  output strobe
);

  wire [w - 1:0] cnt;
  counter # (w) i_counter (.cnt (cnt), .*);
  assign strobe = ~| cnt;  // Same as (cnt == '0)

endmodule
