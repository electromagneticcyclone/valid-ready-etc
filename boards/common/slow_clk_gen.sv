// Asynchronous reset here is needed for one of FPGA boards we use

`include "config.vh"

module slow_clk_gen
# (
  parameter w = 24
)
(
  input  clk,
  input  rst,
  output slow_clk_raw
);

  wire [w - 1:0] cnt;
  counter # (w) i_counter (.cnt (cnt), .*);

  // Note! You have to pass this clock though
  // "global" primitive in Intel FPGA
  // or BUFG  primitive in Xilinx Vivado

  assign slow_clk_raw = cnt [w - 1];

endmodule
