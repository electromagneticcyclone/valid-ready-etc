`include "config.vh"

module tb;

  localparam w = 4;

  //--------------------------------------------------------------------------

  logic           clk;
  logic           rst;

  wire            sender_vld,  receiver_vld;
  wire            sender_rdy,  receiver_rdy;

  wire  [w - 1:0] sender_data, receiver_data;

  logic [w - 1:0] data_init;
  logic [w - 1:0] data_step;

  logic     [7:0] vld_gap_from;
  logic     [7:0] vld_gap_to;

  logic     [6:0] rdy_probability;

  //--------------------------------------------------------------------------

  sender # (.w (w)) i_sender
  (
    .vld  (sender_vld),
    .rdy  (sender_rdy),
    .data (sender_data),
    .*
  );

  receiver # (.w (w)) i_receiver
  (
    .vld  (receiver_vld),
    .rdy  (receiver_rdy),
    .data (receiver_data),
    .*
  );

  //--------------------------------------------------------------------------

  wire           fifo_push;
  wire           fifo_pop;
  wire [w - 1:0] fifo_write_data;
  wire [w - 1:0] fifo_read_data;
  wire           fifo_empty;
  wire           fifo_full;

  //--------------------------------------------------------------------------

  flip_flop_fifo_empty_full_optimized
  # (
    .width (w),
    .depth (10)
  )
  fifo
  (
    .push       ( fifo_push       ),
    .pop        ( fifo_pop        ),
    .write_data ( fifo_write_data ),
    .read_data  ( fifo_read_data  ),
    .empty      ( fifo_empty      ),
    .full       ( fifo_full       ),
    .*
  );

  //--------------------------------------------------------------------------

  assign sender_rdy      = ~ fifo_full;
  assign receiver_vld    = ~ fifo_empty;

  assign fifo_push       =   sender_vld   & sender_rdy;
  assign fifo_pop        =   receiver_vld & receiver_rdy;

  assign fifo_write_data =   sender_data;
  assign receiver_data   =   fifo_read_data;

  //--------------------------------------------------------------------------

  initial
  begin
    clk = 1'b0;

    forever
      # 5 clk = ~ clk;
  end

  //--------------------------------------------------------------------------

  task run_reset;

    rst <= 1'bx;
    repeat (5) @ (posedge clk);
    rst <= 1'b1;
    repeat (5) @ (posedge clk);
    rst <= 1'b0;

  endtask

  //--------------------------------------------------------------------------

  always @ (posedge clk)
    if (~ rst)
    begin
      $write ("%s %s %s",
        sender_vld ? "sv" : "  ",
        sender_rdy ? "sr" : "  ",
        sender_vld & sender_rdy ? "*" : " ");

      if (sender_vld)
        $write (" %h", sender_data);
      else
        $write ("  ");

      $write (" %s %s %s",
        receiver_vld ? "rv" : "  ",
        receiver_rdy ? "rr" : "  ",
        receiver_vld & receiver_rdy ? "*" : " ");

      if (receiver_vld)
        $write (" %h", receiver_data);

      $display;
    end

  //--------------------------------------------------------------------------

  initial
  begin
    `ifdef __ICARUS__
      $dumpvars;
    `endif

    //------------------------------------------------------------------------

    $display ("---  Back-to-back ---");

    data_init        <= 0;
    data_step        <= 1;

    vld_gap_from     <= 0;
    vld_gap_to       <= 0;

    rdy_probability  <= 100;

    run_reset;

    repeat (10) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid, no backpressure ---");

    data_init        <= 0;
    data_step        <= 2;

    vld_gap_from     <= 0;
    vld_gap_to       <= 3;

    rdy_probability  <= 100;

    run_reset;

    repeat (100) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Valid is always on, backpressure ---");

    data_init        <= 0;
    data_step        <= 3;

    vld_gap_from     <= 0;
    vld_gap_to       <= 0;

    rdy_probability  <= 30;

    run_reset;

    repeat (100) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid and backpressure ---");

    data_init        <= 1;
    data_step        <= 4;

    vld_gap_from     <= 0;
    vld_gap_to       <= 3;

    rdy_probability  <= 30;

    run_reset;

    repeat (100) @ (posedge clk);

    //------------------------------------------------------------------------
    $finish;
  end

endmodule
