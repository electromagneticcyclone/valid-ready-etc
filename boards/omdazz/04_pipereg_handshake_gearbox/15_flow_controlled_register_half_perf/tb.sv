`include "config.vh"

module tb;

  localparam w = 4;

  //--------------------------------------------------------------------------

  logic           clk;
  logic           rst;

  wire            sender_vld,  receiver_vld;
  wire            sender_rdy,  receiver_rdy;

  wire  [w - 1:0] sender_data, receiver_data;

  logic [w - 1:0] data_init;
  logic [w - 1:0] data_step;

  logic     [7:0] vld_gap_from;
  logic     [7:0] vld_gap_to;

  logic     [6:0] rdy_probability;

  //--------------------------------------------------------------------------

  sender # (.w (w)) i_sender
  (
    .vld  (sender_vld),
    .rdy  (sender_rdy),
    .data (sender_data),
    .*
  );

  receiver # (.w (w)) i_receiver
  (
    .vld  (receiver_vld),
    .rdy  (receiver_rdy),
    .data (receiver_data),
    .*
  );

  //--------------------------------------------------------------------------

  flow_controlled_register_half_perf # (.w (w)) fc_reg_wo_cp
  (
    .up_vld    ( sender_vld    ),
    .up_rdy    ( sender_rdy    ),
    .up_data   ( sender_data   ),

    .down_vld  ( receiver_vld  ),
    .down_rdy  ( receiver_rdy  ),
    .down_data ( receiver_data ),

    .*
  );

  //--------------------------------------------------------------------------

  initial
  begin
    clk = 1'b0;

    forever
      # 5 clk = ~ clk;
  end

  //--------------------------------------------------------------------------

  task run_reset;

    rst <= 1'bx;
    repeat (5) @ (posedge clk);
    rst <= 1'b1;
    repeat (5) @ (posedge clk);
    rst <= 1'b0;

  endtask

  //--------------------------------------------------------------------------

  always @ (posedge clk)
    if (~ rst)
    begin
      $write ("%s %s %s %h %s %s %s %h",
        sender_vld ? "sv" : "  ",
        sender_rdy ? "sr" : "  ",
        sender_vld & sender_rdy ? "*" : " ",
        sender_data,

        receiver_vld ? "sv" : "  ",
        receiver_rdy ? "sr" : "  ",
        receiver_vld & receiver_rdy ? "*" : " ",
        receiver_data );

      $display;
    end

  //--------------------------------------------------------------------------

  initial
  begin
    `ifdef __ICARUS__
      $dumpvars;
    `endif

    //------------------------------------------------------------------------

    $display ("---  Back-to-back ---");

    data_init        <= 0;
    data_step        <= 1;

    vld_gap_from     <= 0;
    vld_gap_to       <= 0;

    rdy_probability  <= 100;

    run_reset;

    repeat (10) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid, no backpressure ---");

    data_init        <= 0;
    data_step        <= 2;

    vld_gap_from     <= 0;
    vld_gap_to       <= 3;

    rdy_probability  <= 100;

    run_reset;

    repeat (100) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Valid is always on, backpressure ---");

    data_init        <= 0;
    data_step        <= 3;

    vld_gap_from     <= 0;
    vld_gap_to       <= 0;

    rdy_probability  <= 30;

    run_reset;

    repeat (100) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid and backpressure ---");

    data_init        <= 1;
    data_step        <= 4;

    vld_gap_from     <= 0;
    vld_gap_to       <= 3;

    rdy_probability  <= 30;

    run_reset;

    repeat (100) @ (posedge clk);

    //------------------------------------------------------------------------
    $finish;
  end

endmodule
