`include "config.vh"

module tb;

  localparam w = 4;

  //--------------------------------------------------------------------------

  logic           clk;
  logic           rst;

  wire            vld;
  wire            rdy;

  wire  [w - 1:0] data;

  logic [w - 1:0] data_init;
  logic [w - 1:0] data_step;

  logic     [7:0] vld_gap_from;
  logic     [7:0] vld_gap_to;

  logic     [6:0] rdy_probability;

  //--------------------------------------------------------------------------

  sender   # (.w (w)) i_sender   (.*);
  receiver # (.w (w)) i_receiver (.*);

  //--------------------------------------------------------------------------

  initial
  begin
    clk = 1'b0;

    forever
      # 5 clk = ~ clk;
  end

  //--------------------------------------------------------------------------

  task run_reset;

    rst <= 1'bx;
    repeat (5) @ (posedge clk);
    rst <= 1'b1;
    repeat (5) @ (posedge clk);
    rst <= 1'b0;

  endtask

  //--------------------------------------------------------------------------

  always @ (posedge clk)
    if (~ rst)
    begin
      $write ("%s %s",
        vld ? "v" : " ",
        rdy ? "r" : " ");

      if (vld & rdy)
        $write (" %h", data);

      $display;
    end

  //--------------------------------------------------------------------------

  initial
  begin
    `ifdef __ICARUS__
      $dumpvars;
    `endif

    //------------------------------------------------------------------------

    $display ("---  Back-to-back ---");

    data_init        <= 0;
    data_step        <= 1;

    vld_gap_from     <= 0;
    vld_gap_to       <= 0;

    rdy_probability  <= 100;

    run_reset;

    repeat (10) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid, no backpressure ---");

    data_init        <= 0;
    data_step        <= 2;

    vld_gap_from     <= 0;
    vld_gap_to       <= 3;

    rdy_probability  <= 100;

    run_reset;

    repeat (100) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Valid is always on, backpressure ---");

    data_init        <= 0;
    data_step        <= 3;

    vld_gap_from     <= 0;
    vld_gap_to       <= 0;

    rdy_probability  <= 30;

    run_reset;

    repeat (100) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid and backpressure ---");

    data_init        <= 1;
    data_step        <= 4;

    vld_gap_from     <= 0;
    vld_gap_to       <= 3;

    rdy_probability  <= 30;

    run_reset;

    repeat (100) @ (posedge clk);

    //------------------------------------------------------------------------
    $finish;
  end

endmodule
