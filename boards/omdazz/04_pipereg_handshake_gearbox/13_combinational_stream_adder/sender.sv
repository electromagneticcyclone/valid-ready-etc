// In simulation-only modules we use synchronous reset

`include "config.vh"

module sender
# (
  parameter w = 4
)
(
  input                  clk,
  input                  rst,

  output                 vld,
  input                  rdy,

  output logic [w - 1:0] data,

  input        [w - 1:0] data_init,
  input        [w - 1:0] data_step,

  input            [7:0] vld_gap_from,
  input            [7:0] vld_gap_to
);

  logic [7:0] cnt;

  always @ (posedge clk)
    if (rst)
      cnt <= '0;
    else if (vld & rdy)
      cnt <= $urandom_range (vld_gap_from, vld_gap_to);
    else if (cnt != '0)
      cnt <= cnt - 8'd1;

  assign vld = (cnt == '0);

  logic [w - 1:0] data_r;

  always @ (posedge clk)
    if (rst)
      data_r <= data_init;
    else if (vld & rdy)
      data_r <= data_r + data_step;

  assign data = vld ? data_r : 'x;

endmodule
