`include "config.vh"

module tb;

  localparam w = 4;

  //--------------------------------------------------------------------------

  logic           clk;
  logic           rst;

  wire            a_vld, b_vld, sum_vld;
  wire            a_rdy, b_rdy, sum_rdy;
  wire  [w - 1:0] a, b, sum;

  logic     [7:0] a_vld_gap_from, b_vld_gap_from;
  logic     [7:0] a_vld_gap_to,   b_vld_gap_to;

  logic     [6:0] sum_rdy_probability;

  //--------------------------------------------------------------------------

  sender # (w) sender_a
  (
    .vld          ( a_vld          ),
    .rdy          ( a_rdy          ),
    .data         ( a              ),

    .data_init    ( 0              ),
    .data_step    ( 1              ),

    .vld_gap_from ( a_vld_gap_from ),
    .vld_gap_to   ( a_vld_gap_to   ),

    .*
  );

  sender # (w) sender_b
  (
    .vld          ( b_vld          ),
    .rdy          ( b_rdy          ),
    .data         ( b              ),

    .data_init    ( 0              ),
    .data_step    ( 'h100          ),

    .vld_gap_from ( b_vld_gap_from ),
    .vld_gap_to   ( b_vld_gap_to   ),

    .*
  );

  combinational_stream_adder # (w) adder (.*);

  receiver # (w) receiver_sum
  (
    .vld             ( sum_vld             ),
    .rdy             ( sum_rdy             ),
    .data            ( sum                 ),

    .data_init       ( 0                   ),
    .data_step       ( 'h101               ),

    .rdy_probability ( sum_rdy_probability ),
    .*
  );

  //--------------------------------------------------------------------------

  initial
  begin
    clk = 1'b0;

    forever
      # 5 clk = ~ clk;
  end

  //--------------------------------------------------------------------------

  task run_reset;

    rst <= 1'bx;
    repeat (5) @ (posedge clk);
    rst <= 1'b1;
    repeat (5) @ (posedge clk);
    rst <= 1'b0;

  endtask

  //--------------------------------------------------------------------------

  always @ (posedge clk)
    if (~ rst)
    begin
      $write ("a %s %s %s",
        a_vld ? "v" : " ",
        a_rdy ? "r" : " ",
        a_vld & a_rdy ? "*" : " ");

      if (a_vld)
        $write (" %h", a);
      else
        $write ("  ");

      $write (" b %s %s %s",
        b_vld ? "v" : " ",
        b_rdy ? "r" : " ",
        b_vld & b_rdy ? "*" : " ");

      if (b_vld)
        $write (" %h", b);
      else
        $write ("  ");

      $write (" sum %s %s %s",
        sum_vld ? "v" : " ",
        sum_rdy ? "r" : " ",
        sum_vld & sum_rdy ? "*" : " ");

      if (sum_vld)
        $write (" %h", sum);

      $display;
    end

  //--------------------------------------------------------------------------

  initial
  begin
    `ifdef __ICARUS__
      $dumpvars;
    `endif

    //------------------------------------------------------------------------

    $display ("---  Back-to-back ---");

    a_vld_gap_from      <= 0;
    a_vld_gap_to        <= 0;
    b_vld_gap_from      <= 0;
    b_vld_gap_to        <= 0;
    sum_rdy_probability <= 100;

    run_reset;
    repeat (10) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid for \"a\", no backpressure ---");

    a_vld_gap_from      <= 1;
    a_vld_gap_to        <= 3;
    b_vld_gap_from      <= 0;
    b_vld_gap_to        <= 0;
    sum_rdy_probability <= 100;

    run_reset;
    repeat (50) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid for \"b\", no backpressure ---");

    a_vld_gap_from      <= 0;
    a_vld_gap_to        <= 0;
    b_vld_gap_from      <= 1;
    b_vld_gap_to        <= 3;
    sum_rdy_probability <= 100;

    run_reset;
    repeat (50) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid for both \"a\" and \"b\", no backpressure ---");

    a_vld_gap_from      <= 0;
    a_vld_gap_to        <= 3;
    b_vld_gap_from      <= 0;
    b_vld_gap_to        <= 3;
    sum_rdy_probability <= 100;

    run_reset;
    repeat (50) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Valid is always on, backpressure ---");

    a_vld_gap_from      <= 0;
    a_vld_gap_to        <= 0;
    b_vld_gap_from      <= 0;
    b_vld_gap_to        <= 0;
    sum_rdy_probability <= 30;

    run_reset;
    repeat (50) @ (posedge clk);

    //------------------------------------------------------------------------

    $display ("---  Gaps in valid for both \"a\" and \"b\", plus backpressure ---");

    a_vld_gap_from      <= 0;
    a_vld_gap_to        <= 3;
    b_vld_gap_from      <= 0;
    b_vld_gap_to        <= 3;
    sum_rdy_probability <= 30;

    run_reset;
    repeat (50) @ (posedge clk);

    //------------------------------------------------------------------------
    $finish;
  end

endmodule
