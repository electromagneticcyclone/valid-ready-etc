module combinational_stream_adder
# (
  parameter w = 4
)
(
  input            clk,
  input            rst,

  input            a_vld,
  output           a_rdy,
  input  [w - 1:0] a,

  input            b_vld,
  output           b_rdy,
  input  [w - 1:0] b,

  output           sum_vld,
  input            sum_rdy,
  output [w - 1:0] sum
);

  assign a_rdy   = a_vld & b_vld & sum_rdy;
  assign b_rdy   = a_rdy;
  assign sum_vld = a_rdy;
  assign sum     = a + b;

endmodule
