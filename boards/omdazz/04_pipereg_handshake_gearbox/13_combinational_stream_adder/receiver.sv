// In simulation-only modules we use synchronous reset

`include "config.vh"

module receiver
# (
  parameter w = 4
)
(
  input                  clk,
  input                  rst,

  input                  vld,
  output logic           rdy,

  input        [w - 1:0] data,

  input        [w - 1:0] data_init,
  input        [w - 1:0] data_step,

  input            [6:0] rdy_probability
);

  always @ (posedge clk)
    rdy <= $urandom_range (0, 99) < rdy_probability;

  logic [w - 1:0] data_r;

  always @ (posedge clk)
    if ($isunknown (rst))
    // Alternative: if (~ (rst === '0 | rst === '1))
    begin
      // Proper reset is required before any checking
      // If no reset, set the expected data to unknown

      data_r <= 'x;
    end
    else if (rst)
    begin
      data_r <= data_init;
    end
    else if (vld & rdy)
    begin
      if (data != data_r)
        $display ("ERROR: Mismatch: received %d, expected %d",
          data, data_r);

      data_r <= data_r + data_step;
    end

endmodule
