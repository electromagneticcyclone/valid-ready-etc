//  Simplified AXI (SAXI) Verification IP
//  Written by Yuri Panchul as a baseline example for educational projects

module axi_testbench;

  import axi_transaction::*;

  //--------------------------------------------------------------------------
  // Signals

  logic         clk;
  logic         rst;

  wire  addr_t  araddr;
  wire  id_t    arid;
  wire          arvalid;
  wire          arready;

  wire  addr_t  awaddr;
  wire          awvalid;
  wire          awready;

  wire  data_t  wdata;
  wire          wvalid;
  wire          wready;

  wire  data_t  rdata;
  wire  id_t    rid;
  wire          rvalid;
  wire          rready;

  wire          bvalid;
  wire          bready;

  //--------------------------------------------------------------------------
  // Instantiations

  axi_master  master  (.*);
  axi_slave   slave   (.*);

  //--------------------------------------------------------------------------
  // Driving clock

  initial
  begin
    clk = '1;
    forever # 50 clk = ~ clk;
  end

  //--------------------------------------------------------------------------
  // Reset sequence

  task reset_sequence;

    rst <= '1;
    repeat (3) @ (posedge clk);
    rst <= '0;

  endtask

  //--------------------------------------------------------------------------

  `define complete_everything wait fork;

  //--------------------------------------------------------------------------
  // Tests

  // TODO:
  //
  // Implement the test that issues write transactions.
  // Each transaction should finish
  // before launching the next one.
  // Use master.run_write (<args>).

  // Example: master.run_write ('h100, 'h123);

  task test_non_pipelined_writes ();

    $display ("*** %m ***");


  endtask

  //--------------------------------------------------------------------------

  // TODO:
  //
  // Implement a sequence of pipelined transactions
  // using master.start_write (<args>).
  //
  // Use $urandom () to generate values for address and data.
  //
  // Use `complete_everything macro at the end of the sequence
  // in order to wait for all forked threads to finish.

  // Example: master.start_write ('h400, 'h123);

  task test_pipelined_writes_back_to_back ();

    $display ("*** %m ***");


  endtask

  //--------------------------------------------------------------------------

  // TODO:
  //
  // Implement a sequence of pipelined write transactions
  // with data transafer delayed relative to the address transfer.
  //
  // Use `complete_everything macro at the end of the sequence
  // in order to wait for all forked threads to finish.
  //
  // An example how to delay data transfer in a transaction:
  // master.start_write ('h700, 'h123, ._data_delay (3));

  task test_write_data_delayed ();

    $display ("*** %m ***");


  endtask

  //--------------------------------------------------------------------------

  // TODO:
  //
  // Implement a sequence of pipelined write transactions
  // with address transfer delayed relative to the data transfer.
  //
  // Use `complete_everything macro at the end of the sequence
  // in order to wait for all forked threads to finish.
  //
  // An example how to delay address transfer in a transaction:
  // master .start_write ('h700, 'h123, ._addr_delay (3));
  //
  // master.start_write ('h700, 'h123, ._addr_delay (3));

  task test_write_addr_delayed ();

    $display ("*** %m ***");


  endtask

  //--------------------------------------------------------------------------

  // The main initial block

  initial
  begin
    // $dumpfile("dump.vcd");
    // $dumpvars;

    reset_sequence ();

    fork
      begin
        test_non_pipelined_writes ();
        test_pipelined_writes_back_to_back ();
        test_write_data_delayed ();
        test_write_addr_delayed ();
      end

      begin
        repeat (1000)
          @ (posedge clk);

        $display ("Timeout: design hangs");
      end
    join_any

    $finish;
  end

endmodule
