`include "config.vh"

`ifndef __ICARUS__

module fifo_coverage
# (
  parameter width         = 8,
            depth         = 2,
            pointer_width = $clog2 (depth),
            counter_width = $clog2 (depth + 1),
            max_ptr       = depth - 1
)
(
  input               clk,
  input               rst,
  input               push,
  input               pop,
  input [width - 1:0] write_data,
  input [width - 1:0] read_data,
  input               empty,
  input               full,

  // The internal signals

  input [counter_width - 1:0] cnt,
  input [pointer_width - 1:0] wr_ptr,
  input [pointer_width - 1:0] rd_ptr
);

  //--------------------------------------------------------------------------
  // Code to record the clock cycles of push and pop.
  // We will use these recorded values later.

  int cycle;

  always @ (posedge clk)
    cycle <= cycle + 1;

  int cycle_push, cycle_pop;

  always @ (posedge clk)
  begin
    if ( push ) cycle_push <= cycle;
    if ( pop  ) cycle_pop  <= cycle;
  end

  //--------------------------------------------------------------------------

  covergroup cvr @ (posedge clk iff ~ rst);

    option.comment      = "Comment for the report: fifo covergroup";
    option.per_instance = 1;

    // Check we observed at least one push and at least a cycle without push

    coverpoint push
    {
      bins push    = { 1 };
      bins no_push = { 0 };
    }

    coverpoint_without_bins_for_illustration: coverpoint push;

    coverpoint pop { bins pop = { 1 }; bins no_pop = { 0 }; }

    // TODO: add coverpoints for empty and full

    // Check we observed push and pop combinations

    cross push, pop;
    cross push, empty;
    cross pop,  full;

    cross push, pop, empty, full
    {
      illegal_bins illegal_pop_empty
      =  binsof (pop)   intersect { 1 }
      && binsof (empty) intersect { 1 };

      // TODO: add other illegal cross bins
    }

    // Transitions

    push_transitions: coverpoint push
    {
      bins push_010             = ( 0 => 1 => 0 );

      // TODO: add bins for push_101 and push_1001

      bins push_101_or_1001     = ( 1 => 0 => 1 ), ( 1 => 0 => 0 => 1 );
      bins push_10001           = ( 1 => 0 [* 3] => 1 );
      bins push_1_from_3_5_0_1  = ( 1 => 0 [* 3:5] => 1 );
    }

    push_pop_transitions: coverpoint { push, pop }
    {
      bins push_010_pop_000             = ( 'b00 => 'b10 => 'b00 );

      // TODO: add bins for the consecutive_3_push_3_pop using [* 3] repetition
      // and for the non_consecutive_3_push_3_pop using [= 3] repetition
    }

    // See IEEE 1800-2017 Standard for SystemVerilog for other transition features,
    // for example "goto repetition"

    // FIFO counter coverage

    coverpoint cnt
    {
      bins zero          = { 0 };
      bins max           = { depth };
      bins in_between [] = { [1 : depth - 1] };

      illegal_bins others = default;
    }

    // FIFO pointer distance coverage

    rd_after_wr_pointer_distance: coverpoint rd_ptr - wr_ptr
      iff (rd_ptr >= wr_ptr)
    {
      bins zero          = { 0 };
      bins one           = { 1 };
      bins max           = { depth - 1 };
      bins in_between [] = { [2 : depth - 2] };

      illegal_bins others = default;
    }

    // TODO: implement wr_after_rd_pointer_distance

    // TODO: implement coverpoint rd_ptr_same_as_wr_ptr

    // Distance in clock cycles between push and pop

    push_pop_distance: coverpoint cycle_pop - cycle_push
    {
      bins same_time    = {  0 };
      bins push_pop     = {  1 };
      bins pop_push     = { -1 };
      bins push_nop_pop = {  2 };
      bins pop_nop_push = { -2 };

      bins push_near_after_pop_together      = { 3, 4, 5 };
      bins push_near_after_pop_individual [] = { 3, 4, 5 };
      bins push_near_after_pop_range      [] = { [3:5] };

      // TODO: implement:
      //
      // pop_near_after_push
      // push_far_from_pop
      // pop_far_from_push
      //
      // A hint: use '$' for far distance
    }

    // Data

    write_data_auto: coverpoint write_data;

    write_data_specific: coverpoint write_data
      iff (push)
    {
      bins          equal_12                      = {   8'h12 };
      bins          equal_12_or_20                = {   8'h12 , 8'h20   };
      bins          from_12_till_20               = { [ 8'h12 : 8'h20 ] };
      bins          from_12_till_20_in_3_bins [3] = { [ 8'h12 : 8'h20 ] };
      ignore_bins   to_ignore                     = { [ 8'h17 : 8'h18 ] };
      bins          mixed                         = { [ 8'h12 : 8'h20 ] , 8'h30 };
      wildcard bins binary_xx1x1x1x               = {   8'b??1?1?1? };

      // TODO: implement hex_2X
    }

  endgroup

  cvr cg = new ();

  final
  begin
    $display ("Total coverage               : %0d", cg.get_coverage ());
    $display ("write_data_specific coverage : %0d", cg.write_data_specific.get_coverage ());
  end

endmodule

`endif // `ifndef __ICARUS__
