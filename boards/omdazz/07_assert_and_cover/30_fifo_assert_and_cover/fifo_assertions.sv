`include "config.vh"

`ifndef __ICARUS__

module fifo_assertions
# (
  parameter width         = 8,
            depth         = 2,
            pointer_width = $clog2 (depth),
            counter_width = $clog2 (depth + 1),
            max_ptr       = depth - 1
)
(
  input               clk,
  input               rst,
  input               push,
  input               pop,
  input [width - 1:0] write_data,
  input [width - 1:0] read_data,
  input               empty,
  input               full,

  // The internal signals

  input [counter_width - 1:0] cnt,
  input [pointer_width - 1:0] wr_ptr,
  input [pointer_width - 1:0] rd_ptr
);

  //--------------------------------------------------------------------------
  // The simplest cover property and assert property

  observe_push:
    cover property
      (@ (posedge clk) disable iff (rst)
        push);

  assert_fifo_after_push_is_not_empty:
    assert property
      (@ (posedge clk) disable iff (rst)
        push |-> ##1 ~ empty);

  // TODO: assert_fifo_after_pop_is_not_full

  //--------------------------------------------------------------------------
  // Consequitive action

  observe_two_pushes_back_to_back:
    cover property
      (@ (posedge clk) disable iff (rst)
        push ##1 push);

  observe_two_pushes_three_cycles_apart:
    cover property
      (@ (posedge clk) disable iff (rst)
        push ##3 push);

  observe_push_and_pop_from_0_till_3_cycles_apart:
    cover property
      (@ (posedge clk) disable iff (rst)
        push ##[0:3] pop);

  // TODO: observe_push_push_full

  assert_push_into_empty_then_pop_without_push_results_in_empty_fifo:
    assert property
      (@ (posedge clk) disable iff (rst)
        empty & push ##1 pop & ~ push |-> ##1 empty);

  // TODO: assert_pop_from_full_then_push_without_pop_results_in_full_fifo

  //--------------------------------------------------------------------------
  // Repetitions

  // Same as: push ##1 push ##1 push ##1 pop

  observe_3_pushes_back_to_back_using_repetitions_then_pop:
    cover property
      (@ (posedge clk) disable iff (rst)
        push [*3] ##1 pop);

  observe_from_3_to_5_pushes_back_to_back_then_pop:
    cover property
      (@ (posedge clk) disable iff (rst | depth < 5)
        push [*3:5] ##1 pop);

  // TODO: assert_3_pushes_into_empty_fifo_without_pops_results_in_counter_equal_3

  //--------------------------------------------------------------------------
  // Sequence "or" and "and"

  assert_push_pop_or_pop_push_do_not_change_non_empty_non_full_status:
    assert property
      (@ (posedge clk) disable iff (rst)
        ~ empty & ~ full
        ##0
        ( push & ~ pop  ##1 pop  & ~ push
          or
          pop  & ~ push ##1 push & ~ pop )
        |-> ##1
        ~ empty & ~ full);

  //--------------------------------------------------------------------------

  sequence seq101001 (sig);
    sig ##1 ~ sig ##1 sig ##1 ~ sig [*2] ##1 sig;
  endsequence

  sequence seq110011 (sig);
    sig [*2] ##1 ~ sig [*2] ##1 sig [*2];
  endsequence

  assert_3_pushes_with_gaps_in_parallel_with_3_pops_with_gaps_do_not_change_non_empty_non_full_status:
    assert property
      (@ (posedge clk) disable iff (rst)
        ~ empty & ~ full
        ##0
        (seq101001 (push) and seq110011 (pop))
        |-> ##1
        ~ empty & ~ full);

  // TODO: implement cover property
  // observed_3_pushes_with_gaps_in_parallel_with_3_pops_with_gaps

  //--------------------------------------------------------------------------
  // Pushes with "goto repetitions" and "throughout"

  three_non_consequitive_pushes_without_pops_result_in_cnt_3_and_non_empty:
    assert property
      (@ (posedge clk) disable iff (rst | depth < 3)
       ~ pop throughout (empty and push [->3]) |-> ##1 ~ empty & cnt == 3);

  //--------------------------------------------------------------------------
  // Using properties with local variables

  property prop_head_after_push_in_empty_fifo_gets_the_same_data;
    logic [width - 1:0] data;
    (push & empty, data = write_data)
    |-> ##1
    ~ empty & read_data == data;
  endproperty

  check_that_head_after_push_in_empty_fifo_gets_the_same_data:
    assert property
      (@ (posedge clk) disable iff (rst)
       prop_head_after_push_in_empty_fifo_gets_the_same_data);

  // TODO: check_that_head_after_n_push_in_empty_fifo_without_pops_gets_correct_data

endmodule

`endif // `ifndef __ICARUS__
