onerror { exit }
coverage save -onexit coverage.ucdb
vlib work
vlog +incdir+../../../../common ../*.sv
vsim work.tb
onfinish exit
run -all
