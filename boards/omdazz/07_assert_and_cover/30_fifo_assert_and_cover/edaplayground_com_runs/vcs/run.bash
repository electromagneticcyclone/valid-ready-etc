#!/usr/bin/env bash

vcs -R \
-licqueue \
-sverilog \
-timescale=1ns/1ns \
+vcs+flush+all \
flip_flop_fifo_with_counter.sv \
fifo_assertions.sv \
fifo_coverage.sv \
fifo_monitor.sv \
tb.sv

urg -dir simv.vdb -format both
ls -l urgReport/*txt
cat urgReport/*txt
