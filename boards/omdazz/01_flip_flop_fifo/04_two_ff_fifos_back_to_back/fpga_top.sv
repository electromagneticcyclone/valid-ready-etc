// Asynchronous reset here is needed for the FPGA board we use

`include "config.vh"

`ifndef SIMULATION

module fpga_top
(
  input              clk,
  input              reset_n,

  input        [3:0] key_sw,
  output       [3:0] led,

  output logic [7:0] abcdefgh,
  output       [3:0] digit,

  output             buzzer,

  output             hsync,
  output             vsync,
  output       [2:0] rgb
);

  //--------------------------------------------------------------------------

  wire rst = ~ reset_n;

  assign led    = '1;
  assign buzzer = 1'b1;
  assign hsync  = 1'b1;
  assign vsync  = 1'b1;
  assign rgb    = 3'b0;

  //--------------------------------------------------------------------------

  wire slow_clk_raw, slow_clk;

  slow_clk_gen # (26) i_slow_clk_gen (.slow_clk_raw (slow_clk_raw), .*);

  // "global" is Intel FPGA-specific primitive to route
  // a signal coming from data into clock tree

  global i_global (.in (slow_clk_raw), .out (slow_clk));

  //--------------------------------------------------------------------------

  localparam fifo_width = 4, fifo_depth = 5;

  wire [fifo_width - 1:0] write_data;
  wire [fifo_width - 1:0] read_data;
  wire empty, full;

  // Either of two leftmost keys is pressed
  wire push = ~ full  & key_sw [3:2] != 2'b11;

  // Either of two rightmost keys is pressed
  wire pop  = ~ empty & key_sw [1:0] != 2'b11;

  wire [63:0] debug;

  //--------------------------------------------------------------------------

  counter # (fifo_width) i_counter (.clk (slow_clk), .cnt (write_data), .*);

  two_ff_fifos_back_to_back
  # (
    .width (fifo_width),
    .depth (fifo_depth)
  )
  i_fifo (.clk (slow_clk), .*);

  wire [3:0] wr_ptr = debug [19 + 32 : 16 + 32];
  wire [3:0] rd_ptr = debug [       3:       0];

  seven_segment_4_digits i_display
  (
    .clk    ( clk ),
    .number ( { write_data , wr_ptr , rd_ptr , read_data } ),
    .dots   ( { 1'b0       , full   , empty  , 1'b0      } ),
    .*
  );

endmodule

`endif  // ifndef SIMULATION
