// Asynchronous reset here is needed for the FPGA board we use

`include "config.vh"

`ifndef SIMULATION

module fpga_top
(
  input              clk,
  input              reset_n,

  input        [3:0] key_sw,
  output       [3:0] led,

  output logic [7:0] abcdefgh,
  output       [3:0] digit,

  output             buzzer,

  output             hsync,
  output             vsync,
  output       [2:0] rgb
);

  //--------------------------------------------------------------------------

  wire rst = ~ reset_n;

  assign led    = '1;
  assign buzzer = 1'b1;
  assign hsync  = 1'b1;
  assign vsync  = 1'b1;
  assign rgb    = 3'b0;

  //--------------------------------------------------------------------------

  wire enable;
  strobe_gen # (26) i_strobe_gen (.strobe (enable), .*);

  //--------------------------------------------------------------------------

  localparam fifo_width = 4, fifo_depth = 5;

  wire [fifo_width - 1:0] write_data;
  wire [fifo_width - 1:0] read_data;
  wire empty, full;

  // Either of two leftmost keys is pressed
  wire push = ~ full  & key_sw [3:2] != 2'b11;

  // Either of two rightmost keys is pressed
  wire pop  = ~ empty & key_sw [1:0] != 2'b11;

  wire [31:0] debug;

  //--------------------------------------------------------------------------

  counter_with_enable # (fifo_width) i_counter (.cnt (write_data), .*);

  flip_flop_fifo_with_counter
  # (
    .width (fifo_width),
    .depth (fifo_depth)
  )
  i_fifo (.*);

  wire [3:0] wr_ptr = debug [19:16];
  wire [3:0] rd_ptr = debug [ 3: 0];

  seven_segment_4_digits i_display
  (
    .number ( { write_data , wr_ptr , rd_ptr , read_data } ),
    .dots   ( { 1'b0       , full   , empty  , 1'b0      } ),
    .*
  );

endmodule

`endif  // ifndef SIMULATION
