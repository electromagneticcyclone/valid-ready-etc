# Цифровой синтез

Упражнения к главе 10 учебника [Цифровой синтез: практический курс](https://dmkpress.com/catalog/electronics/circuit_design/978-5-97060-850-0/).
Исходная репозитория: [github.com/MIPSfpga/digital-design-lab-manual](https://github.com/MIPSfpga/digital-design-lab-manual).

![Структура конвейера в упражнении](https://www.silicon-russia.com/wp-content/uploads/2017/11/05_pow_5_pipeline.png)
