`include "config.vh"

module tb;

  localparam n_fc_reg = 7,
             w = 4;

  //--------------------------------------------------------------------------

  logic clk;
  logic rst;

  //--------------------------------------------------------------------------

  initial
  begin
    clk = 1'b0;

    forever
      # 5 clk = ~ clk;
  end

  initial
  begin
    repeat (100000) @ (posedge clk);
    $display ("Timeout!");
    $finish;
  end

  //--------------------------------------------------------------------------

  task run_reset;

    rst <= 1'b1;
    repeat (5) @ (posedge clk);
    rst <= 1'b0;

  endtask

  //--------------------------------------------------------------------------

  logic [$clog2 (100) - 1:0] vld_probability;
  logic [$clog2 (100) - 1:0] rdy_probability;

  logic log_performance_counters;

  //--------------------------------------------------------------------------

  generate

    genvar i_fc_reg;

    for (i_fc_reg = 1; i_fc_reg <= n_fc_reg; i_fc_reg ++)
    begin : sub_tb
      tb_for_fc_reg
      # (
        .which_fc_reg (i_fc_reg),
        .w (w)
      )
      i_tb_for_fc_reg (.*);
    end

  endgenerate

  //--------------------------------------------------------------------------

  task test (string name, int vld_prob, int rdy_prob, int cycles = 100);

    $display ("--- %s ---", name);

    vld_probability <= vld_prob;
    rdy_probability <= rdy_prob;

    run_reset;

    repeat (cycles) @ (posedge clk);

    log_performance_counters <= '1;
    @ (posedge clk);
    log_performance_counters <= '0;

  endtask

  //--------------------------------------------------------------------------

  initial
  begin
    `ifdef __ICARUS__
      $dumpvars;
    `endif

    //------------------------------------------------------------------------

    //     name                     vld_probability   rdy_probability

    test ( "Back-to-back"                     , 100 , 100 );
    test ( "Gaps in valid, no backpressure"   ,  30 , 100 );
    test ( "Valid is always on, backpressure" , 100 ,  30 );
    test ( "Gaps in valid and backpressure"   ,  30 ,  30 );

    test ("Long random", 50, 50, 10000 ); // cycles

    //------------------------------------------------------------------------

    $finish;
  end

endmodule
