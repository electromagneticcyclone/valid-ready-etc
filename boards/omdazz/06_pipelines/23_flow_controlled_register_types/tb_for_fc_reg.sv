`include "config.vh"

module tb_for_fc_reg
# (
  parameter which_fc_reg = 0,
            w = 0
)
(
  input clk,
  input rst,

  input [$clog2 (100) - 1:0] vld_probability,
  input [$clog2 (100) - 1:0] rdy_probability,

  input log_performance_counters
);

  //----------------------------------------------------------------------

  wire           up_vld;
  wire           up_rdy;
  wire [w - 1:0] up_data;

  wire           down_vld;
  wire           down_rdy;
  wire [w - 1:0] down_data;

  //----------------------------------------------------------------------

  sender # (.w (w)) up
  (
    .vld  (up_vld),
    .rdy  (up_rdy),
    .data (up_data),
    .*
  );

  receiver # (.w (w)) down
  (
    .vld  (down_vld),
    .rdy  (down_rdy),
    .data (down_data),
    .*
  );

  //----------------------------------------------------------------------

  generate

    case (which_fc_reg)
    1: fcr_1_single_allows_back_to_back           # (.w (w)) fc_reg (.*);
    2: fcr_2_single_half_perf_no_comb_path        # (.w (w)) fc_reg (.*);
    3: fcr_3_single_for_pipes_with_global_stall   # (.w (w)) fc_reg (.*);
    4: fcr_4_wrapped_2_deep_fifo                  # (.w (w)) fc_reg (.*);
    5: fcr_5_skid_buffer_from_laforest            # (.w (w)) fc_reg (.*);
    6: fcr_6_double_buffer_from_dally_harting     # (.w (w)) fc_reg (.*);
    7: fcr_7_pipe_stomach_from_digital_synthesis  # (.w (w)) fc_reg (.*);
    endcase

  endgenerate

  //----------------------------------------------------------------------
  // Logging

  string log_name, log_file_name;
  int f;

  initial
  begin
    case (which_fc_reg)
    1: log_name = "single_allows_back_to_back";
    2: log_name = "single_half_perf_no_comb_path";
    3: log_name = "single_for_pipes_with_global_stall";
    4: log_name = "wrapped_2_deep_fifo";
    5: log_name = "skid_buffer_from_laforest";
    6: log_name = "double_buffer_from_dally_harting";
    7: log_name = "pipe_stomach_from_digital_synthesis";

    default:
      begin
        $display ("Invalid which_fc_reg=%d", which_fc_reg);
        $finish;
      end
    endcase

    $sformat (log_file_name, "log_%0d_%s.txt", which_fc_reg, log_name);

    f = $fopen (log_file_name, "w");

    if (f == 0)
    begin
      $display ("error: cannot open file \"%s\" for writing", log_file_name);
      $finish;
    end
  end

  final
    if (f != 0)
      $fclose (f);

  //----------------------------------------------------------------------

  always @ (posedge clk)
    if (~ rst)
    begin
      $fwrite (f, "%s %s",
        up_vld ? "uv" : "  ",
        up_rdy ? "ur" : "  ");

      if (up_vld & up_rdy)
        $fwrite (f, " %h", up_data);
      else
        $fwrite (f, "  ");

      $fwrite (f, " %s %s",
        down_vld ? "dv" : "  ",
        down_rdy ? "dr" : "  ");

      if (down_vld & down_rdy)
        $fwrite (f, " %h", down_data);
      else
        $fwrite (f, "  ");

      $fdisplay (f);
    end

  //----------------------------------------------------------------------
  // Performance counters

  logic [32:0] n_cycles, up_cnt, down_cnt;

  always @ (posedge clk)
    if (rst)
    begin
      n_cycles <= '0;
      up_cnt   <= '0;
      down_cnt <= '0;
    end
    else
    begin
      n_cycles <= n_cycles + 1'd1;

      if (up_vld & up_rdy)
        up_cnt <= up_cnt + 1'd1;

      if (down_vld & down_rdy)
        down_cnt <= down_cnt + 1'd1;
    end

  //----------------------------------------------------------------------

  string message;

  always @ (posedge clk)
    if (log_performance_counters)
    begin
      $sformat (message,
        "number of transfers : up %0d down %0d per %0d cycles",
        up_cnt, down_cnt, n_cycles);

      $fdisplay (f, "performance statistics : vld %3d %% rdy %3d %% : %s",
        vld_probability, rdy_probability, message);

      $display ("fcr_%0d_%-35s : %s",
        which_fc_reg, log_name, message);
    end

endmodule
