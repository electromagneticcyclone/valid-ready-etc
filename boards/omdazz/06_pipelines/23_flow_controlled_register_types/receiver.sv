// In simulation-only modules we use synchronous reset

`include "config.vh"

module receiver
# (
  parameter w = 0
)
(
  input        clk,
  input        rst,

  input        vld,
  output logic rdy,

  input  [w - 1:0] data,

  input  [$clog2 (100) - 1:0] rdy_probability
);

  always @ (posedge clk)
    rdy <= $urandom_range (0, 99) < rdy_probability;

  logic [w - 1:0] data_r;

  always @ (posedge clk)
    if (rst)
    begin
      data_r <= '0;
    end
    else if (vld & rdy)
    begin
      if (data != data_r)
        $display ("ERROR: Mismatch: received %d, expected %d",
          data, data_r);

      data_r <= data_r + 1'd1;
    end

endmodule
