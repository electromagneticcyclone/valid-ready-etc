// In simulation-only modules we use synchronous reset

`include "config.vh"

module sender
# (
  parameter w = 0
)
(
  input  clk,
  input  rst,

  output logic vld,
  input        rdy,

  output logic [w - 1:0] data,

  input  [$clog2 (100) - 1:0] vld_probability
);

  always @ (posedge clk)
    if (rst)
      vld <= '0;
    else if (~ vld | rdy)
      vld <= $urandom_range (0, 99) < vld_probability;

  logic [w - 1:0] data_r;

  always @ (posedge clk)
    if (rst)
      data_r <= '0;
    else if (vld & rdy)
      data_r <= data_r + 1'd1;

  assign data = vld ? data_r : 'x;

endmodule
