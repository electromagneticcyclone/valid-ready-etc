// Asynchronous reset here is needed for one of FPGA boards we use

`include "config.vh"

// This solution is Yuri Panchul's version of Kurt Baty's design
// described in Digital Synthesis book
// https://dmkpress.com/catalog/electronics/circuit_design/978-5-97060-850-0/
// It is equivalent to 2-deep FIFO

module fcr_7_pipe_stomach_from_digital_synthesis
# (
  parameter w = 0
)
(
  input                  clk,
  input                  rst,

  input                  up_vld,
  output                 up_rdy,
  input        [w - 1:0] up_data,

  output logic           down_vld,
  input                  down_rdy,
  output logic [w - 1:0] down_data
);

    wire stomach_load, dataout_load, dataout_unload;
    reg  mux_select;

    wire [w - 1:0] data_in;
    reg  [w - 1:0] stomach_out, mux_out, data_out;

    reg data_in_dataout, data_in_stomach;

    assign data_in = up_data;

    assign down_vld = data_in_dataout;

    assign dataout_unload = down_vld & down_rdy;

    assign dataout_load =
           (up_vld & (~ data_in_dataout | dataout_unload))
         | (data_in_stomach & dataout_unload);

    assign stomach_load =
            up_vld
        & ~ data_in_stomach
        &   (data_in_dataout & ~ dataout_unload);

    assign up_rdy = ~ data_in_stomach;

    always @ (posedge clk or posedge rst)
        if (rst)
        begin
            data_in_stomach <= 1'b0;
            data_in_dataout <= 1'b0;
        end
        else
        begin
            data_in_stomach <=    stomach_load
                               | (data_in_stomach & ~ dataout_unload);

            data_in_dataout <=    dataout_load
                               |  data_in_stomach
                               | (data_in_dataout & ~ dataout_unload);
        end

    always @ (posedge clk or posedge rst)
        if (rst)
            mux_select <= 1'b0;
        else
            mux_select <= stomach_load | (mux_select & ~ dataout_load);

    always @ (posedge clk)
        if (stomach_load)
            stomach_out <= data_in;

    always @*
        mux_out = mux_select ? stomach_out : data_in;

    always @ (posedge clk)
        if (dataout_load)
            data_out <= mux_out;

    assign down_data = data_out;

endmodule
