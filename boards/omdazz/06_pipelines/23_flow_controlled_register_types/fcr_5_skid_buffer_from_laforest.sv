// Asynchronous reset here is needed for one of FPGA boards we use

`include "config.vh"

// It is equivalent to 2-deep FIFO

module fcr_5_skid_buffer_from_laforest
# (
  parameter w = 0
)
(
  input                  clk,
  input                  rst,

  input                  up_vld,
  output                 up_rdy,
  input        [w - 1:0] up_data,

  output logic           down_vld,
  input                  down_rdy,
  output logic [w - 1:0] down_data
);

  Pipeline_Skid_Buffer
  # (
    .WORD_WIDTH (w)
  )
  skid
  (
    .clock        ( clk       ),
    .clear        ( rst       ),

    .input_valid  ( up_vld    ),
    .input_ready  ( up_rdy    ),
    .input_data   ( up_data   ),

    .output_valid ( down_vld  ),
    .output_ready ( down_rdy  ),
    .output_data  ( down_data )
  );

endmodule
