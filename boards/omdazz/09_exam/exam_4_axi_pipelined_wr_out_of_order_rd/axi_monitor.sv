//  Simplified AXI (SAXI) Verification IP
//  Written by Yuri Panchul as a baseline example for educational projects

import axi_transaction::*;

module axi_monitor
(
  input         clk,
  input         rst,

  input addr_t  araddr,
  input id_t    arid,
  input         arvalid,
  input         arready,

  input addr_t  awaddr,
  input         awvalid,
  input         awready,

  input data_t  wdata,
  input         wvalid,
  input         wready,

  input data_t  rdata,
  input id_t    rid,
  input         rvalid,
  input         rready,

  input         bvalid,
  input         bready
);

  //--------------------------------------------------------------------------
  // Queues, counters and memories

  addr_t wr_addr_queue [$];
  data_t wr_data_queue [$];

  data_t rd_data_array [n_ids][$];
  int unsigned wr_resp_counter;

  data_t shadow_memory [addr_t];  // A sparse array

  //--------------------------------------------------------------------------
  // Dumping shadow memory at the end of simulation

  final
  begin
    $display ("monitor: the shadow memory dump");

    // TODO: Implement the dumping code.
    // You can use the slave code as a reference.


    $display ("monitor: end of the shadow memory dump");
  end

  //--------------------------------------------------------------------------
  // Main processing

  int unsigned cycle;
  data_t expected_rdata;

  always @ (posedge clk)
    if (rst)
    begin
      cycle = 0;  // Blocking assignment here is intentional

      //----------------------------------------------------------------------
      // Clearing the queues, counters and shadow_memory

      // TODO: Implement this fragment of the code.
      // You can use the slave code as a reference.

    end
    else
    begin
      cycle ++;  // Blocking assignment here is intentional

      //----------------------------------------------------------------------
      // Getting data from the master channels.
      // Performing shadow memory operations.
      // Write operation has priority over read operation.

      if (awvalid & awready)
      begin
        $display ("%0d monitor: write address arrived: %h",
          cycle, awaddr);

        wr_addr_queue.push_back (awaddr);
      end

      // TODO:
      //
      // Implement getting write data
      // from the corresponding channel
      // and writing a value to the shadow memory.
      //
      // You can use the slave code as a reference.


      //----------------------------------------------------------------------

      if (arvalid & arready)
      begin
        if (! shadow_memory.exists (araddr))
        begin
          $display ("%0d monitor: attempt to read from the shadow memory location %h which was not written",
            cycle, araddr);

          rd_data_array [arid].push_back ('x);
        end
        else
        begin
          $display ("%0d monitor: read shadow memory [%h] = %h, id=%0d",
            cycle, araddr, shadow_memory [araddr], arid);

          rd_data_array [arid].push_back (shadow_memory [araddr]);
        end
      end

      //----------------------------------------------------------------------
      // Checking the responses from the slave

      if (bvalid & bready)
      begin
        if (wr_resp_counter == 0)
        begin
          $display ("%0d monitor: unexpected write response", cycle);
        end
        else
        begin
          $display ("%0d monitor: write response", cycle);
          wr_resp_counter --;
        end
      end

      //----------------------------------------------------------------------

      // TODO:
      //
      // Implement checking the read data coming from the slave
      // against the value read from the shadow memory
      // when the read address arrived.

    end

endmodule
