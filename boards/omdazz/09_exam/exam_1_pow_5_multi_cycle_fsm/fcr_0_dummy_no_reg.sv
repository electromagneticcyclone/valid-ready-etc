`include "config.vh"

module fcr_0_dummy_no_reg
# (
  parameter w = 0
)
(
  input            clk,
  input            rst,

  input            up_vld,
  output           up_rdy,
  input  [w - 1:0] up_data,

  output           down_vld,
  input            down_rdy,
  output [w - 1:0] down_data
);

  assign up_rdy    = down_rdy;
  assign down_vld  = up_vld;
  assign down_data = up_data;

endmodule
