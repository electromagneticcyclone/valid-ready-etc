// Asynchronous reset here is needed for one of FPGA boards we use

`include "config.vh"

module pow_5_multi_cycle_fsm
# (
  parameter width = 0
)
(
  input                clk,
  input                rst,

  input                up_vld,    // upstream
  output               up_rdy,
  input  [width - 1:0] up_data,

  output               down_vld,  // downstream
  input                down_rdy,
  output [width - 1:0] down_data
);

  // TODO:
  //
  // Design a parameterized module that computes n ** 5 sequentially.
  // The requirements:
  //
  // 1. The design should use only one multiplication operation
  // and infer a single multiplier when synthesized in hardware.
  //
  // 2. The design should be able to produce a result every 5 clock cycles
  // when down_rdy and up_vld are both kept high.
  //
  // 3. The design should not break when backpressure is applied,
  // down_rdy can go down any clock cycle.
  //
  // 4. The design shold work on FPGA board.
  //
  // 5. The module should be fully compable with the same testbench
  // that was used for pow_5_single_cycle and pow_5_pipelined.
  //
  // 6. Bonus point 1: Compare the clock frequency and the resource
  // utilization of this design with pow_5_single_cycle and pow_5_pipelined.
  // Make sure that the path through the multiplier starts from a flop
  // and ends in a flop.
  //
  // 7. Bonus point 2: Write a concurrent assertion that checks:
  // if down_rdy is kept high throughout the computation
  // and we assert up_vld high,
  // then we are going to get down_vld high
  // and the correct down_data = up_data ** 5
  // within 5-7 clock cycles after up_vld=1 is accepted with up_rdy=1.
  //
  // You can look into golden_log.txt and golden_wave.png,
  // the log and waveforms produced by our solution, for your reference.

  logic [width - 1:0] input_number;
  logic [        2:0] state_counter;

  always_comb begin
    up_rdy   = (state_counter == 1'b0);
    down_vld = (state_counter == 3'd5);
  end

  always_ff @(posedge clk or posedge rst) begin
    if (rst) begin
      down_data     <= 0;
      state_counter <= 0;
    end else begin
      input_number <= (up_vld & up_rdy) ? up_data : input_number;
      down_data <= (up_vld & up_rdy) ?
          up_data :
          (down_vld & ~down_rdy) ?
              down_data :
              down_data * input_number;
      state_counter <= up_rdy ?
          up_vld :
          down_vld ?
              (down_rdy ? 0 : state_counter) :
              state_counter + 1;
    end
  end

endmodule
