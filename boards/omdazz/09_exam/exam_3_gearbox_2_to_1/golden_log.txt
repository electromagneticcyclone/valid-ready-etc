VCD info: dumpfile dump.vcd opened for output.
VCD warning: $dumpvars: Unsupported argument type (vpiPackage)
time      10 cycle     0
time      20 cycle     1
time      30 cycle     2
time      40 cycle     3 rst
time      50 cycle     4 rst
*** Run back-to-back
time      60 cycle     5 rst
time      70 cycle     6     up_vld           down_vld down_rdy A
time      80 cycle     7     up_vld up_rdy AB down_vld down_rdy B
time      90 cycle     8     up_vld           down_vld down_rdy O
time     100 cycle     9     up_vld up_rdy OG down_vld down_rdy G
time     110 cycle    10     up_vld           down_vld down_rdy A
time     120 cycle    11     up_vld up_rdy AF down_vld down_rdy F
time     130 cycle    12     up_vld           down_vld down_rdy N
time     140 cycle    13     up_vld up_rdy NU down_vld down_rdy U
time     150 cycle    14     up_vld           down_vld down_rdy F
*** Random up_vld and down_rdy
time     160 cycle    15     up_vld up_rdy FA down_vld down_rdy A
time     170 cycle    16     up_vld           down_vld down_rdy T
time     180 cycle    17     up_vld up_rdy TQ down_vld down_rdy Q
time     190 cycle    18                               down_rdy
time     200 cycle    19                               down_rdy
time     210 cycle    20     up_vld           down_vld
time     220 cycle    21     up_vld           down_vld down_rdy K
time     230 cycle    22     up_vld           down_vld
time     240 cycle    23     up_vld           down_vld
time     250 cycle    24     up_vld           down_vld
time     260 cycle    25     up_vld up_rdy KK down_vld down_rdy K
time     270 cycle    26                               down_rdy
time     280 cycle    27     up_vld           down_vld
time     290 cycle    28     up_vld           down_vld
time     300 cycle    29     up_vld           down_vld
time     310 cycle    30     up_vld           down_vld
time     320 cycle    31     up_vld           down_vld down_rdy C
time     330 cycle    32     up_vld           down_vld
time     340 cycle    33     up_vld up_rdy CF down_vld down_rdy F
time     350 cycle    34     up_vld           down_vld down_rdy S
time     360 cycle    35     up_vld           down_vld
time     370 cycle    36     up_vld           down_vld
time     380 cycle    37     up_vld up_rdy SK down_vld down_rdy K
time     390 cycle    38     up_vld           down_vld down_rdy N
time     400 cycle    39     up_vld           down_vld
time     410 cycle    40     up_vld           down_vld
time     420 cycle    41     up_vld           down_vld
time     430 cycle    42     up_vld up_rdy NK down_vld down_rdy K
time     440 cycle    43     up_vld           down_vld down_rdy K
time     450 cycle    44     up_vld           down_vld
time     460 cycle    45     up_vld           down_vld
time     470 cycle    46     up_vld           down_vld
time     480 cycle    47     up_vld           down_vld
time     490 cycle    48     up_vld           down_vld
time     500 cycle    49     up_vld up_rdy KT down_vld down_rdy T
time     510 cycle    50     up_vld           down_vld down_rdy R
time     520 cycle    51     up_vld           down_vld
time     530 cycle    52     up_vld           down_vld
time     540 cycle    53     up_vld up_rdy RD down_vld down_rdy D
time     550 cycle    54     up_vld           down_vld down_rdy R
time     560 cycle    55     up_vld up_rdy RQ down_vld down_rdy Q
time     570 cycle    56     up_vld           down_vld
time     580 cycle    57     up_vld           down_vld
time     590 cycle    58     up_vld           down_vld down_rdy J
time     600 cycle    59     up_vld up_rdy JI down_vld down_rdy I
time     610 cycle    60     up_vld           down_vld
time     620 cycle    61     up_vld           down_vld
time     630 cycle    62     up_vld           down_vld
time     640 cycle    63     up_vld           down_vld
time     650 cycle    64     up_vld           down_vld
*** Draining the pipeline: up_vld=0, down_rdy=1
time     660 cycle    65     up_vld           down_vld down_rdy K
time     670 cycle    66     up_vld up_rdy KE down_vld down_rdy E
time     680 cycle    67                               down_rdy
time     690 cycle    68                               down_rdy
time     700 cycle    69                               down_rdy
time     710 cycle    70                               down_rdy
time     720 cycle    71                               down_rdy
time     730 cycle    72                               down_rdy
time     740 cycle    73                               down_rdy
time     750 cycle    74                               down_rdy
time     760 cycle    75                               down_rdy
time     770 cycle    76                               down_rdy


number of transfers : up 15 down 30 per 71 cycles
