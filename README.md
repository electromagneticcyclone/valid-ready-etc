# Valid, Ready Etc

От базовых знаний - к профессиональным умениям
Упражнения для изучения проектирования и верификации цифровых микросхем

## Программное обеспечение для работы с упражнениями

* Скачать среду разработки Intel® Quartus® Prime Lite Edition Design Software Version 22.1:
[для Linux](https://www.intel.com/content/www/us/en/software-kit/757261/intel-quartus-prime-lite-edition-design-software-version-22-1-for-linux.html)
и [для Windows](https://www.intel.com/content/www/us/en/software-kit/757262/intel-quartus-prime-lite-edition-design-software-version-22-1-for-windows.html)
* [Git для Windows](https://git-scm.com/download/win)
* Симулятор Icarus Verilog:
[исходники на GitHub](https://github.com/steveicarus/iverilog),
[скачать для Windows (версию 11.00)](https://bleyer.org/icarus/).
Для многих дистрибутов Линукса (Lubuntu, ALT Linux, Simply Linux, Green Linux)
Icarus Verilog можно установить
с помощью "sudo apt-get install iverilog" или "sudo yum install iverilog".
* Программа просмотра временных диаграмм для использования с Icarus Verilog:
[исходники на GitHub](https://github.com/gtkwave/gtkwave).
Для Windows GTKWave входит в дистрибутив Icarus Verilog.
Для многих дистрибутов Линукса (Lubuntu, ALT Linux, Simply Linux, Green Linux)
GTKWave можно установить
с помощью "sudo apt-get install gtkwave" или "sudo yum install gtkwave".
* Вебсайт и приложение для рисования временных диаграмм [WaveDrom](https://wavedrom.com)
* Вебсайты для рисования микроархитектурных диаграмм:
[Lucidspark](https://lucidspark.com) и [draw.io](https://draw.io)

## Другие образовательные проекты

* [Пайка, ПЛИС и Линукс](https://gitflic.ru/project/yuri-panchul/fpga-soldering-camp)
* [Digital Design & Verification in Central Asia](https://github.com/ddvca)
* [YRV-Plus](https://github.com/yuri-panchul/yrv-plus)
